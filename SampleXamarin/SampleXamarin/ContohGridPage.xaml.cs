﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ContohGridPage : ContentPage
    {
        public ContohGridPage()
        {
            InitializeComponent();
            btnHalMain.Clicked += BtnHalMain_Clicked;
        }

        private void BtnHalMain_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new MainPage());
        }
    }
}