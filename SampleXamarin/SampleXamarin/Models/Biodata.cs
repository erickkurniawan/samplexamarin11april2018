﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleXamarin.Models
{
    public class Biodata
    {
        public int PersonID { get; set; }
        public string Nama { get; set; }
        public string Alamat { get; set; }
        public string Email { get; set; }
        public string Telp { get; set; }
        public int Umur { get; set; }
    }
}
