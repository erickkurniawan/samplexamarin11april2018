﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SampleXamarin.Models
{
    public class Produk
    {
        public string ProdukID { get; set; }
        public string Nama { get; set; }
        public string Deskripsi { get; set; }
        public int Jumlah { get; set; }
        public double Harga { get; set; }
        public string Gambar { get; set; }
    }
}
