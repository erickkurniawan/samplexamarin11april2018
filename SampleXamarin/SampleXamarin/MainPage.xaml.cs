﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace SampleXamarin
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            menuSimple.Clicked += MenuSimple_Clicked;
            menuProduk.Clicked += MenuProduk_Clicked;
            menuCustom.Clicked += MenuCustom_Clicked;
		}

        private void MenuCustom_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new CustomListViewPage());
        }

        private void MenuProduk_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new ProdukListPage());
        }

        private void MenuSimple_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new SimplePage());
        }

        private void BtnHitung_Clicked(object sender, EventArgs e)
        {
            double bil1 = Convert.ToDouble(txtBil1.Text);
            double bil2 = Convert.ToDouble(txtBil2.Text);

            double hasil = 0;

            var btn = (Button)sender;
            if (btn.Text == "Tambah")
            {
                hasil = bil1 + bil2;
            }
            else if (btn.Text == "Kali")
            {
                hasil = bil1 * bil2;
            }
            else
            {
                hasil = bil1 - bil2;
            }
            DisplayAlert("Keterangan", "Hasilnya adalah: " + hasil, "OK");
        }
    }
}
