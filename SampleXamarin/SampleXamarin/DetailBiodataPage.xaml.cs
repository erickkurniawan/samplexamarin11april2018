﻿using SampleXamarin.Models;
using SampleXamarin.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailBiodataPage : ContentPage
    {
        private BiodataServices biodataServices;
        public DetailBiodataPage()
        {
            InitializeComponent();
            biodataServices = new BiodataServices();
            btnUpdate.Clicked += BtnUpdate_Clicked;
            btnDelete.Clicked += BtnDelete_Clicked;
        }

        private async void BtnDelete_Clicked(object sender, EventArgs e)
        {
            var hasil = await DisplayAlert("Konfirmasi", "Apakah anda yakin untuk delete data?", "Yes", "No");
            if (hasil)
            {
                try
                {
                    var boolDelete = await biodataServices.DeleteBiodata(Convert.ToInt32(txtPersonID.Text));
                    if (boolDelete)
                    {
                        await DisplayAlert("Keterangan", "Data Biodata berhasil delete", "OK");
                        await Navigation.PopAsync();
                    }
                    else
                    {
                        await DisplayAlert("Keterangan", "Data gagal delete !", "OK");
                    }
                }
                catch (Exception ex)
                {
                    await DisplayAlert("Keterangan", ex.Message, "OK");
                }
            }
        }

        private async void BtnUpdate_Clicked(object sender, EventArgs e)
        {
            var editBio = new Biodata
            {
                Nama = txtNama.Text,
                Alamat = txtAlamat.Text,
                Email = txtEmail.Text,
                Telp = txtTelp.Text,
                Umur = Convert.ToInt32(txtUmur.Text)
            };
            try
            {
                var boolEdit = await biodataServices.UpdateBiodata(Convert.ToInt32(txtPersonID.Text),
                    editBio);

                if (boolEdit)
                {
                    await DisplayAlert("Keterangan", "Data Biodata berhasil edit", "OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Keterangan", "Data gagal edit !", "OK");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Keterangan", ex.Message, "OK");
            }
        }
    }
}