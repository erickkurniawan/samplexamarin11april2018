﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using SampleXamarin.Models;

namespace SampleXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ProdukListPage : ContentPage
    {
        public ProdukListPage()
        {
            InitializeComponent();
            List<Produk> lstProduk = new List<Produk>
            {
                new Produk {ProdukID="1",Nama="Asus Zen Book",
                    Deskripsi ="Laptop Asus Zenbook Ultrabook",
                    Harga =12000000,Jumlah=10,Gambar="monkey1.png"},
                new Produk {ProdukID="2",Nama="Thinkpad X1 Carbon",
                    Deskripsi ="Laptop Thinkpad X1 Carbon",
                    Harga =15000000,Jumlah=9,Gambar="icon.png"},
                new Produk {ProdukID="3",Nama="Asus ROG",
                    Deskripsi ="Laptop Asus Republic Of Game",
                    Harga =15000000,Jumlah=11,Gambar="monkey1.png"}
            };
            lvProduk.ItemsSource = lstProduk;
        }
    }
}