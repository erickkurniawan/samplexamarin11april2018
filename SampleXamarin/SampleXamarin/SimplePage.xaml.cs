﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace SampleXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SimplePage : ContentPage
    {
        public SimplePage()
        {
            InitializeComponent();

            List<string> lstNama = new List<string> { "Laptop Asus Zen Book","Acer 210",
                "Lenovo Thinkpad X1","MSI Gaming","Asus ROG" };
            lvData.ItemsSource = lstNama;
            lvData.ItemTapped += LvData_ItemTapped;
        }

        private void LvData_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var strPilih = e.Item.ToString();
            DisplayAlert("Keterangan", "Anda memilih : " + strPilih, "OK");
            ((ListView)sender).SelectedItem = null;
        }
    }
}