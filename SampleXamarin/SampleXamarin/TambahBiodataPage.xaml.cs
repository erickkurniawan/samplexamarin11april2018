﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SampleXamarin.Models;
using SampleXamarin.Services;

namespace SampleXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TambahBiodataPage : ContentPage
    {
        private BiodataServices biodataServices;
        public TambahBiodataPage()
        {
            InitializeComponent();
            biodataServices = new BiodataServices();
            btnTambah.Clicked += BtnTambah_Clicked;
        }

        private async void BtnTambah_Clicked(object sender, EventArgs e)
        {
            var newBiodata = new Biodata
            {
                Nama = txtNama.Text,
                Alamat = txtAlamat.Text,
                Email = txtEmail.Text,
                Telp = txtTelp.Text,
                Umur = Convert.ToInt32(txtUmur.Text)
            };

            try
            {
                var boolTambah = await biodataServices.TambahBiodata(newBiodata);
                if(boolTambah)
                {
                    await DisplayAlert("Keterangan", "Data Biodata berhasil ditambah","OK");
                    await Navigation.PopAsync();
                }
                else
                {
                    await DisplayAlert("Keterangan", "Data gagal ditambah !", "OK");
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("Keterangan", ex.Message, "OK");
            }
        }
    }
}