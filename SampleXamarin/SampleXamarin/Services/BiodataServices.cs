﻿using System;
using System.Collections.Generic;
using System.Text;

using System.Net.Http;
using SampleXamarin.Models;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace SampleXamarin.Services
{
    public class BiodataServices
    {
        private HttpClient _client;
        public IEnumerable<Biodata> BiodataItems { get; set; }

        public BiodataServices()
        {
            _client = new HttpClient();
        }

        public async Task<IEnumerable<Biodata>> GetAllBiodata()
        {
            var uri = new Uri("http://samplebackend.azurewebsites.net/api/Biodata");
            try
            {
                var response = await _client.GetAsync(uri);
                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    BiodataItems = JsonConvert.DeserializeObject<List<Biodata>>(content);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return BiodataItems;
        }

        public async Task<bool> TambahBiodata(Biodata biodata)
        {
            var uri = new Uri("http://samplebackend.azurewebsites.net/api/Biodata");
            try
            {
                var json = JsonConvert.SerializeObject(biodata);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PostAsync(uri, content);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> UpdateBiodata(int id,Biodata biodata)
        {
            var uri = new Uri($"http://samplebackend.azurewebsites.net/api/Biodata/{id}");
            try
            {
                var json = JsonConvert.SerializeObject(biodata);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                var response = await _client.PutAsync(uri, content);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public async Task<bool> DeleteBiodata(int id)
        {
            var uri = new Uri($"http://samplebackend.azurewebsites.net/api/Biodata/{id}");
            try
            {
                var response = await _client.DeleteAsync(uri);
                if (response.IsSuccessStatusCode)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
