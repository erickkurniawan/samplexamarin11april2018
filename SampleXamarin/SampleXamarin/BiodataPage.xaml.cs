﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

using SampleXamarin.Services;
using SampleXamarin.Models;

namespace SampleXamarin
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BiodataPage : ContentPage
    {
        private BiodataServices myServices;
        public BiodataPage()
        {
            InitializeComponent();
            myServices = new BiodataServices();
            menuTambah.Clicked += MenuTambah_Clicked;
            lvBiodata.ItemTapped += LvBiodata_ItemTapped;
        }

        private async void LvBiodata_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var bioItem = (Biodata)e.Item;
            var detailPage = new DetailBiodataPage();
            detailPage.BindingContext = bioItem;
            await Navigation.PushAsync(detailPage);
        }

        private void MenuTambah_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TambahBiodataPage());
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            try
            {
                lvBiodata.ItemsSource = await myServices.GetAllBiodata();
            }
            catch (Exception ex)
            {
                await DisplayAlert("Keterangan", ex.Message, "OK");
            }
        }
    }
}